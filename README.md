# unshift

![Terminal SVG "screenshot"](term.svg)

A Python script that reverses bitwise operations to find individual components of bitmasks.

Requires:

- CVC4 with Python bindings
- pycparser
- huepy
